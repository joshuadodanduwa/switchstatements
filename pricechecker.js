let fruitInput = ""


fruitInput = prompt("which fruit's price do you want to check?\nApples, Bananas, Kiwifruit or Oranges?")

//lowercase
fruitInput = fruitInput.toLowerCase()


//switch statements

switch(fruitInput) {

    case "apples":
        console.log("Apples cost $1.25 per kg")
        break
    
    case "bananas":
        console.log("Bananas cost $3.15 per kg")
        break
    
    case "kiwifruit":
        console.log("Kiwifruit cost $4.65 per kg")
        break
    
    case "oranges":
        console.log("Oranges cost $2.75 per kg")
        break
    
    default:
        console.log("incorrect input.")
        break

}