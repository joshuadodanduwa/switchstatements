let petInput = ""


petInput = prompt("I have four imaginary pets: Cat, Dog, Fish and Rabbit.\nGuess my favourite:")

//lowercase
petInput = petInput.toLowerCase()


//switch statements

switch(petInput) {

    case "cat":
        console.log("My cat is not my favourite")
        break

    case "dog":
        console.log("My dog is my favourite!")
        break

    case "fish":
        console.log("My fish is not my favourite!")
        break
    
    case "rabbit":
        console.log("My rabbit is not my favourite!")
        break

    default:
        console.log("Not a correct input. Try again.")
        break

}